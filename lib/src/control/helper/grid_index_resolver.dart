part of datagrid;

@protected
class _GridIndexResolver {
  static int getHeaderIndex(_DataGridSettings dataGridSettings) {
    if (dataGridSettings == null) {
      return 0;
    }

    final headerIndex = dataGridSettings.headerLineCount - 1;
    return headerIndex < 0 ? 0 : headerIndex;
  }

  static int resolveToGridVisibleColumnIndex(
      _DataGridSettings dataGridSettings, int columnIndex) {
    if (dataGridSettings == null) {
      return -1;
    }

    final indentColumnCount = 0;
    return columnIndex - indentColumnCount;
  }

  static int resolveToRowIndex(
      _DataGridSettings dataGridSettings, int rowIndex) {
    if (dataGridSettings != null &&
        dataGridSettings.container != null &&
        rowIndex != -1) {
      rowIndex = rowIndex +
          _GridIndexResolver.resolveStartIndexBasedOnPosition(dataGridSettings);
      if (rowIndex >= 0) {
        return rowIndex;
      } else if (rowIndex < 0) {
        return 0;
      }
    } else {
      return 0;
    }

    return -1;
  }

  static int resolveStartIndexBasedOnPosition(
      _DataGridSettings dataGridSettings) {
    final topBodyCount = 0;
    return dataGridSettings != null
        ? dataGridSettings.headerLineCount + topBodyCount
        : 0;
  }

  static int resolveToRecordIndex(
      _DataGridSettings dataGridSettings, int rowIndex) {
    if (dataGridSettings != null &&
        dataGridSettings.container != null &&
        rowIndex != -1) {
      rowIndex = rowIndex -
          _GridIndexResolver.resolveStartIndexBasedOnPosition(dataGridSettings);
      if (rowIndex >= 0) {
        return rowIndex;
      } else if (rowIndex < 0) {
        return 0;
      }
    } else {
      return 0;
    }

    return -1;
  }

  static int resolveToStartColumnIndex(_DataGridSettings dataGridSettings) {
    final startIndex = 0;
    return startIndex;
  }

  static RowColumnIndex resolveToRowColumnIndex(
      _DataGridSettings dataGridSettings, RowColumnIndex rowColumnIndex) {
    final rowIndex = _GridIndexResolver.resolveToRecordIndex(
        dataGridSettings, rowColumnIndex.rowIndex);
    final columnIndex = _GridIndexResolver.resolveToGridVisibleColumnIndex(
        dataGridSettings, rowColumnIndex.columnIndex);
    return RowColumnIndex(rowIndex, columnIndex);
  }

  static int resolveToScrollColumnIndex(
          _DataGridSettings dataGridSettings, int gridColumnIndex) =>
      gridColumnIndex;
}
