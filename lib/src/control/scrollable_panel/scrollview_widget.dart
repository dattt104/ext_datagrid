part of datagrid;

class _ScrollViewWidget extends StatefulWidget {
  const _ScrollViewWidget(
      {@required this.dataGridStateDetails, this.width, this.height});

  final _DataGridStateDetails dataGridStateDetails;
  final double width;
  final double height;

  @override
  State<StatefulWidget> createState() => _ScrollViewWidgetState();
}

class _ScrollViewWidgetState extends State<_ScrollViewWidget> {
  ScrollController _verticalController;
  ScrollController _horizontalController;
  FocusNode _dataGridFocusNode;
  double _width = 0.0;
  double _height = 0.0;
  double _scrollX = 0.0;
  double _scrollY = 0.0;
  bool _isScrolling = false;

  @override
  void initState() {
    _verticalController = ScrollController()..addListener(_verticalListener);
    _horizontalController = ScrollController()
      ..addListener(_horizontalListener);

    _height = widget.height;
    _width = widget.width;
    _dataGridSettings
      ..verticalController = _verticalController
      ..horizontalController = _horizontalController;

    if (_dataGridSettings.rowSelectionManager != null) {
      _dataGridSettings.rowSelectionManager
          .addListener(_handleSectionController);
    }

    if (_dataGridFocusNode == null) {
      _dataGridFocusNode = FocusNode(onKey: _handleFocusKeyOperation);
      _dataGridSettings.dataGridFocusNode = _dataGridFocusNode;
    }

    super.initState();
  }

  _DataGridSettings get _dataGridSettings => widget.dataGridStateDetails();

  _VisualContainerHelper get _container => _dataGridSettings.container;

  _RowGenerator get _rowGenerator => _dataGridSettings.rowGenerator;

  SelectionManagerBase get _rowSelectionManager =>
      _dataGridSettings.rowSelectionManager;

  void _verticalListener() {
    setState(() {
      final newValue = _verticalController.offset;
      _container.verticalOffset = newValue;
      _scrollY = newValue;
      _container.setRowHeights();
      _isScrolling = true;
      _container._isDirty = true;
    });
  }

  void _horizontalListener() {
    setState(() {
      final newValue = _horizontalController.offset;
      _container.horizontalOffset = newValue;
      _scrollX = newValue;
      _dataGridSettings.columnSizer._refresh(widget.width);
      _isScrolling = true;
      _container._isDirty = true;
    });
  }

  void _updateAxis() {
    final double width = _width;
    final double height = _height + _dataGridSettings.headerRowHeight;
    _container.updateAxis(Size(width, height));
  }

  void _setHorizontalOffset() {
    if (_container._needToSetHorizontalOffset) {
      _container.horizontalOffset = _scrollX;
      _container.scrollColumns.markDirty();
    }

    _container._needToSetHorizontalOffset = false;
  }

  void _updateColumnSizer() {
    final columnSizer = _dataGridSettings.columnSizer;
    if (columnSizer != null) {
      if (columnSizer._isColumnSizerLoadedInitially) {
        columnSizer
          .._initialRefresh(widget.width)
          .._isColumnSizerLoadedInitially = false;
      } else {
        columnSizer._refresh(widget.width);
      }
    }
  }

  void _ensureWidgets() {
    if (_dataGridSettings.source.dataSource == null) {
      return;
    }

    if (!_container._isPreGenerator) {
      _container.preGenerateItems();
    } else {
      if (_container._needToRefreshColumn) {
        _ensureItems(true);
        _container._needToRefreshColumn = false;
      } else {
        _ensureItems(false);
      }
    }
  }

  void _ensureItems(bool needToRefresh) {
    final _VisibleLinesCollection visibleRows =
        _container.scrollRows.getVisibleLines();
    final _VisibleLinesCollection visibleColumns =
        _SfDataGridHelper.getVisibleLines(_rowGenerator.dataGridStateDetails());

    if (_container._isGridLoaded && visibleColumns.isNotEmpty) {
      _rowGenerator._ensureRows(visibleRows, visibleColumns);
    }

    if (needToRefresh) {
      if (visibleColumns.isNotEmpty) {
        _rowGenerator._ensureColumns(visibleColumns);
      }
    }
  }

  void _addScrollView(List<Widget> children) {
    final double _extentWidth = _container.extentWidth;
    final double _headerRowHeight = _dataGridSettings.headerRowHeight;
    final double _extentHeight = _container.extentHeight - _headerRowHeight;

    final Widget scrollView = Scrollbar(
      controller: _horizontalController,
      child: SingleChildScrollView(
          controller: _horizontalController,
          scrollDirection: Axis.horizontal,
          physics: const AlwaysScrollableScrollPhysics(),
          child: ConstrainedBox(
              constraints: BoxConstraints(
                minWidth: _width,
              ),
              child: Scrollbar(
                controller: _verticalController,
                child: SingleChildScrollView(
                  controller: _verticalController,
                  physics: const AlwaysScrollableScrollPhysics(),
                  child: ConstrainedBox(
                      constraints: BoxConstraints(
                        minHeight: min(_height, _extentHeight),
                      ),
                      child: _VisualContainer(
                          isDirty: _container._isDirty,
                          rowGenerator: _rowGenerator,
                          containerSize: Size(
                              max(_width, _extentWidth),
                              _extentHeight > _height
                                  ? _extentHeight
                                  : min(_height, _extentHeight)))),
                ),
              ))),
    );

    final Positioned wrapScrollView = Positioned.fill(
      top: _headerRowHeight,
      child: scrollView,
    );

    children.add(wrapScrollView);
  }

  void _addHeaderRows(List<Widget> children) {
    List<Widget> _buildHeaderRows() => _rowGenerator.items
        .where((rows) => rows.rowRegion == RowRegion.header)
        .map<Widget>((dataRow) => _HeaderCellsWidget(
              key: dataRow._key,
              dataRow: dataRow,
              isDirty: _container._isDirty || dataRow._isDirty,
            ))
        .toList(growable: false);

    if (_rowGenerator.items.isNotEmpty) {
      final List<Widget> headerRows = _buildHeaderRows();

      for (final row in headerRows) {
        final Positioned header = Positioned.directional(
            textDirection: _dataGridSettings.textDirection,
            start: (_dataGridSettings.textDirection == TextDirection.ltr)
                ? -_container.horizontalOffset
                : _container.horizontalOffset,
            top: 0.0,
            height: _dataGridSettings.headerRowHeight,
            width: _width,
            child: row);
        children.add(header);
      }
    }
  }

  void _handleSectionController() async {
    setState(() {
      /* Rebuild the DataGrid when the selection or currentcell is processed. */
    });
  }

  bool _handleFocusKeyOperation(FocusNode focusNode, RawKeyEvent e) {
    bool needToMoveFocus() {
      final _DataGridSettings dataGridSettings = _dataGridSettings;

      bool canAllowToRemoveFocus(int rowIndex, int columnIndex) =>
          (dataGridSettings.navigationMode == GridNavigationMode.cell &&
              _dataGridSettings.currentCell.rowIndex == rowIndex &&
              _dataGridSettings.currentCell.columnIndex == columnIndex) ||
          (dataGridSettings.navigationMode == GridNavigationMode.row &&
              _dataGridSettings.currentCell.rowIndex == rowIndex);

      if (e.isShiftPressed) {
        final firstRowIndex =
            _SelectionHelper.getFirstRowIndex(_dataGridSettings);
        final firstCellIndex =
            _SelectionHelper.getFirstCellIndex(_dataGridSettings);

        if (canAllowToRemoveFocus(firstRowIndex, firstCellIndex)) {
          return false;
        } else {
          return true;
        }
      } else {
        final lastRowIndex =
            _SelectionHelper.getLastNavigatingRowIndex(_dataGridSettings);
        final lastCellIndex =
            _SelectionHelper.getLastCellIndex(_dataGridSettings);

        if (canAllowToRemoveFocus(lastRowIndex, lastCellIndex)) {
          return false;
        } else {
          return true;
        }
      }
    }

    if (e.logicalKey == LogicalKeyboardKey.tab) {
      return needToMoveFocus();
    } else {
      return true;
    }
  }

  void _handleKeyOperation(RawKeyEvent e) {
    if (e.runtimeType == RawKeyDownEvent) {
      _rowSelectionManager.handleKeyEvent(e);
    }
  }

  @override
  void didUpdateWidget(_ScrollViewWidget oldWidget) {
    super.didUpdateWidget(oldWidget);

    if (oldWidget.width != widget.width ||
        oldWidget.height != widget.height ||
        _container._needToSetHorizontalOffset) {
      _width = widget.width;
      _height = widget.height;
      _container
        .._needToSetHorizontalOffset = true
        .._isDirty = true;
      // FLUT-2047 Need to mark all visible rows height as dirty when DataGrid
      // size is changed.
      if (oldWidget.width != widget.width &&
          _dataGridSettings.onQueryRowHeight != null) {
        _container.rowHeightManager.reset();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    if (_container._isDirty && !_isScrolling) {
      _updateAxis();
      _updateColumnSizer();
      _container
        ..setRowHeights()
        .._needToRefreshColumn = true;
    }

    if (_container._needToSetHorizontalOffset) {
      _setHorizontalOffset();
    }

    if (_container._isDirty) {
      _ensureWidgets();
    }

    final List<Positioned> children = [];

    _addHeaderRows(children);

    _addScrollView(children);

    _container._isDirty = false;
    _isScrolling = false;

    return RawKeyboardListener(
        focusNode: _dataGridFocusNode,
        onKey: _handleKeyOperation,
        child: Container(
            height: _height,
            width: _width,
            decoration: const BoxDecoration(
              color: Colors.transparent,
            ),
            // Remove this ClipRect widget and uncomment the below line.
            // when the below mentioned issue resolved from framework side.
            // https://github.com/flutter/flutter/issues/50508
            // clipBehavior: Clip.antiAlias,
            child: ClipRect(
                clipBehavior: Clip.antiAlias,
                clipper: _DataGridClipper(),
                child: Stack(
                    fit: StackFit.passthrough,
                    children: List.from(children)))));
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DoubleProperty('_scrollY', _scrollY));
  }

  @override
  void dispose() {
    if (_verticalController != null) {
      _verticalController
        ..removeListener(_verticalListener)
        ..dispose();
    }

    if (_horizontalController != null) {
      _horizontalController
        ..removeListener(_horizontalListener)
        ..dispose();
    }

    super.dispose();
  }
}

// Remove the below custom clipper class.
// when the below mentioned issue resolved from framework side.
// https://github.com/flutter/flutter/issues/50508
class _DataGridClipper extends CustomClipper<Rect> {
  @override
  Rect getClip(Size size) => Rect.fromLTWH(0.0, 0.0, size.width, size.height);

  @override
  bool shouldReclip(_DataGridClipper oldClipper) => false;
}
