part of datagrid;

/// Holds the arguments for the [SfDataGrid.onQueryRowStyle] callback.
class QueryRowStyleArgs {
  /// Creates the [QueryRowStyleArgs] with the specified [rowIndex].
  QueryRowStyleArgs({@required this.rowIndex});

  /// An index of a row in which the style is applied.
  final int rowIndex;

  /// Decides how the style is applied when selection and row style are applied.
  StylePreference stylePreference = StylePreference.selection;
}

/// Holds the arguments for the [SfDataGrid.onQueryCellStyle] callback.
class QueryCellStyleArgs {
  /// Creates the [QueryRowStyleArgs] with the specified [gridColumn],
  /// [rowIndex], [columnIndex], [cellValue] and [displayText].
  QueryCellStyleArgs(
      {@required this.column,
      @required this.rowIndex,
      @required this.columnIndex,
      @required this.cellValue,
      @required this.displayText});

  /// A [GridColumn] in which the style is applied.
  final GridColumn column;

  /// A row index of a cell.
  final int rowIndex;

  /// A column index of a cell.
  final int columnIndex;

  /// The value of a cell. Typically it is from underlying datasource.
  final Object cellValue;

  /// The display text of a cell. Typically it is the formatted text when number
  /// format or date format is applied for column.
  String displayText;

  /// Decides how the style is applied when selection and cell style are
  /// applied.
  StylePreference stylePreference = StylePreference.selection;
}
