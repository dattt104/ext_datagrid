part of datagrid;

/// A widget which displays in the header cells.
class GridHeaderCell extends GridCell {
  /// Creates the [GridHeaderCell] for [SfDataGrid] widget.
  const GridHeaderCell(
      {@required Key key,
      DataCellBase dataCell,
      EdgeInsets padding,
      Color backgroundColor,
      bool isDirty,
      Alignment alignment,
      Widget child})
      : super(
            key: key,
            child: child,
            dataCell: dataCell,
            padding: padding,
            alignment: alignment,
            backgroundColor: backgroundColor,
            isDirty: isDirty);
  @override
  State<StatefulWidget> createState() => _GridHeaderCellState();
}

class _GridHeaderCellState extends State<GridHeaderCell> {
  @override
  Widget build(BuildContext context) {
    final _DataGridSettings dataGridSettings =
        widget.dataCell?._dataRow?._dataGridStateDetails();

    final Widget headerCellWidget =
        (dataGridSettings != null && dataGridSettings.headerCellBuilder != null)
            ? dataGridSettings.headerCellBuilder(
                context, widget.dataCell.gridColumn)
            : null;

    return GridCell(
      key: widget.key,
      dataCell: widget.dataCell,
      padding: widget.padding,
      backgroundColor: widget.backgroundColor,
      isDirty: widget.isDirty,
      alignment: headerCellWidget == null ? widget.alignment : null,
      child: headerCellWidget ?? widget.child,
    );
  }
}
