part of datagrid;

typedef _DataGridSourceListener = void Function(
    {RowColumnIndex rowColumnIndex, String propertyName});

/// A datasource for obtaining the row data for the [SfDataGrid].
///
/// The following APIs are mandatory to process the data,
/// * [dataSource] - The number of rows in a datagrid and row selection depends
/// on the [dataSource]. So, set the collection required for datagrid in
/// [dataSource].
/// * [getCellValue] - The data needed for the cells is obtained from
/// [getCellValue].
///
/// Call the [notifyDataSourceListeners] when performing CRUD in the underlying
/// datasource. When updating data in a cell, pass the corresponding
/// [RowColumnIndex] to the [notifyDataSourceListeners].
///
/// [DataGridSource] objects are expected to be long-lived, not recreated with
/// each build.
/// ``` dart
/// final List<Employee> _employees = <Employee>[];
///
/// class EmployeeDataSource extends DataGridSource
/// {
///   @override
///   List<Object> get dataSource => _employees;
///
///   @override
///   getCellValue(int rowIndex, String columnName){
///     switch (columnName) {
///       case 'id':
///         return employees[rowIndex].id;
///         break;
///       case 'name':
///         return employees[rowIndex].name;
///         break;
///       case 'salary':
///         return employees[rowIndex].salary;
///         break;
///       case 'designation':
///         return employees[rowIndex].designation;
///         break;
///       default:
///         return ' ';
///         break;
///     }
///   }
/// ```
abstract class DataGridSource extends _DataGridSourceChangeNotifier {
  /// Creates the [DataGridSource] for [SfDataGrid] widget.
  DataGridSource({List<Object> dataSource}) {
    _dataSource = dataSource ?? [];
  }

  /// An underlying datasource to populate the rows for [SfDataGrid].
  ///
  /// This property should be set to process the selection operation.
  List<Object> get dataSource => _dataSource;
  List<Object> _dataSource;

  /// Called to obtain the data for the cells.
  Object getCellValue(int rowIndex, String columnName) => null;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is DataGridSource && runtimeType == other.runtimeType;

  @override
  int get hashCode {
    final List<Object> _hashList = [this, dataSource];
    return hashList(_hashList);
  }
}

/// Controls a [SfDataGrid] widget.
///
/// This can be used to control the selection and currentcell operations such
/// as programmatically select a row or rows, move the currentcell to
/// required position.
///
/// DataGrid controllers are typically stored as member variables in [State]
/// objects and are reused in each [State.build].
class DataGridController extends _DataGridSourceChangeNotifier {
  /// Creates the [DataGridController] with the [selectedIndex], [selectedRow]
  /// and [selectedRows].
  DataGridController(
      {int selectedIndex, Object selectedRow, List<Object> selectedRows})
      : _selectedRow = selectedRow,
        _selectedIndex = selectedIndex,
        _selectedRows = selectedRows ?? [] {
    _currentCell = RowColumnIndex(-1, -1);
  }

  _DataGridStateDetails _dataGridStateDetails;

  /// The collection of objects that contains object of corresponding
  /// to the selected rows in [SfDataGrid].
  List<Object> get selectedRows => _selectedRows;
  List<Object> _selectedRows = [];

  /// The collection of objects that contains object of corresponding
  /// to the selected rows in [SfDataGrid].
  set selectedRows(List<Object> newSelectedRows) {
    if (_selectedRows == newSelectedRows) {
      return;
    }

    _selectedRows = newSelectedRows;
    notifyDataSourceListeners(propertyName: 'selectedRows');
  }

  /// An index of the corresponding selected row.
  int get selectedIndex => _selectedIndex;
  int _selectedIndex;

  /// An index of the corresponding selected row.
  set selectedIndex(int newSelectedIndex) {
    if (_selectedIndex == newSelectedIndex) {
      return;
    }

    _selectedIndex = newSelectedIndex;
    notifyDataSourceListeners(propertyName: 'selectedIndex');
  }

  /// An object of the corresponding selected row.
  ///
  /// The given object must be given from the underlying datasource of the
  /// [SfDataGrid].
  Object get selectedRow => _selectedRow;
  Object _selectedRow;

  /// An object of the corresponding selected row.
  ///
  /// The given object must be given from the underlying datasource of the
  /// [SfDataGrid].
  set selectedRow(Object newSelectedRow) {
    if (_selectedRow == newSelectedRow) {
      return;
    }

    _selectedRow = newSelectedRow;
    notifyDataSourceListeners(propertyName: 'selectedRow');
  }

  /// A cell which is currently active.
  ///
  /// This is used to identify the currently active cell to process the
  /// key navigation.
  RowColumnIndex get currentCell => _currentCell;
  RowColumnIndex _currentCell;

  /// Moves the currentcell to the specified cell coordinates.
  void moveCurrentCellTo(RowColumnIndex rowColumnIndex) {
    final _DataGridSettings dataGridSettings = _dataGridStateDetails();
    if (dataGridSettings != null &&
        rowColumnIndex != null &&
        rowColumnIndex != RowColumnIndex(-1, -1) &&
        dataGridSettings.selectionMode != SelectionMode.none &&
        dataGridSettings.navigationMode != GridNavigationMode.row) {
      final rowIndex = _GridIndexResolver.resolveToRowIndex(
          dataGridSettings, rowColumnIndex.rowIndex);
      final columnIndex = _GridIndexResolver.resolveToGridVisibleColumnIndex(
          dataGridSettings, rowColumnIndex.columnIndex);

      final rowSelectionController = dataGridSettings.rowSelectionManager;
      if (rowSelectionController is RowSelectionManager) {
        rowSelectionController._processSelectionAndCurrentCell(
            dataGridSettings, RowColumnIndex(rowIndex, columnIndex),
            isProgrammatic: true);
      }
    }
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is DataGridController && runtimeType == other.runtimeType;

  @override
  int get hashCode {
    final List<Object> _hashList = [
      selectedRows,
      selectedRow,
      selectedIndex,
      currentCell
    ];
    return hashList(_hashList);
  }
}

class _DataGridSourceChangeNotifier {
  ObserverList<_DataGridSourceListener> _listeners =
      ObserverList<_DataGridSourceListener>();

  void addListener(_DataGridSourceListener listener) {
    _listeners.add(listener);
  }

  // ignore: unused_element
  bool get _hasListeners => _listeners.isNotEmpty;

  void notifyDataSourceListeners(
      {RowColumnIndex rowColumnIndex, String propertyName}) {
    for (final listener in _listeners) {
      listener(rowColumnIndex: rowColumnIndex, propertyName: propertyName);
    }
  }

  void removeListener(_DataGridSourceListener listener) {
    _listeners.remove(listener);
  }

  @mustCallSuper
  void dispose() {
    _listeners = null;
  }
}
