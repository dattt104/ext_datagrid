// import 'package:flutter/material.dart';

// void main() {
//   runApp(_MyApp());
// }

// class _MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Syncfusion DataGrid Demo',
//       theme: ThemeData(primarySwatch: Colors.blue),
//       home: _MyHomePage(),
//     );
//   }
// }

// class _MyHomePage extends StatefulWidget {
//   _MyHomePage({Key key}) : super(key: key);

//   @override
//   _MyHomePageState createState() => _MyHomePageState();
// }

// final List<_Employee> _employees = <_Employee>[];

// final EmployeeDataSource _employeeDataSource = EmployeeDataSource();

// class _MyHomePageState extends State<_MyHomePage> {
//   @override
//   void initState() {
//     super.initState();
//     populateData();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: const Text('DataGrid'),
//       ),
//       body: SfDataGrid(
//         source: _employeeDataSource,
//         columns: <GridColumn>[
//           GridNumericColumn(mappingName: 'id', headerText: 'ID'),
//           GridTextColumn(mappingName: 'name', headerText: 'Name'),
//           GridTextColumn(mappingName: 'designation', headerText: 'Designation'),
//           GridNumericColumn(mappingName: 'salary', headerText: 'Salary'),
//         ],
//       ),
//     );
//   }

//   void populateData() {
//     _employees.add(_Employee(10001, 'James', 'Project Lead', 20000));
//     _employees.add(_Employee(10002, 'Kathryn', 'Manager', 30000));
//     _employees.add(_Employee(10003, 'Lara', 'Developer', 15000));
//     _employees.add(_Employee(10004, 'Michael', 'Designer', 15000));
//     _employees.add(_Employee(10005, 'Martin', 'Developer', 15000));
//     _employees.add(_Employee(10006, 'Newberry', 'Developer', 15000));
//     _employees.add(_Employee(10007, 'Balnc', 'Developer', 15000));
//     _employees.add(_Employee(10008, 'Perry', 'Developer', 15000));
//     _employees.add(_Employee(10009, 'Gable', 'Developer', 15000));
//     _employees.add(_Employee(10010, 'Grimes', 'Developer', 15000));
//   }
// }

// class _Employee {
//   _Employee(this.id, this.name, this.designation, this.salary);
//   final int id;
//   final String name;
//   final String designation;
//   final int salary;
// }

// //ignore: public_member_api_docs
// class EmployeeDataSource extends DataGridSource {
//   @override
//   List<Object> get dataSource => _employees;

//   @override
//   Object getCellValue(int rowIndex, String columnName) {
//     switch (columnName) {
//       case 'id':
//         return _employees[rowIndex].id;
//         break;
//       case 'name':
//         return _employees[rowIndex].name;
//         break;
//       case 'salary':
//         return _employees[rowIndex].salary;
//         break;
//       case 'designation':
//         return _employees[rowIndex].designation;
//         break;
//       default:
//         return ' ';
//         break;
//     }
//   }
// }
